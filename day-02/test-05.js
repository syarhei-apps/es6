var prefix = "user_";

var o = {
    [prefix + "foo"]() {
        return "foo";
    },
    [prefix + "bar"]() {
        return this[prefix + "foo"]();
    }
};

console.log(o[prefix + "bar"]());

