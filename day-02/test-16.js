var snowman = "\u2603";
console.log( snowman );			// "☃"

var gclef = "\uD834\uDD1E";
console.log( gclef );			// "𝄞"

var gclef = "\u{1D11E}";
console.log( gclef );			// "𝄞"

var snowman = "☃";
console.log(snowman.length);					// 1

var gclef = "𝄞";
console.log(gclef.normalize().length);					// 2

var s1 =  " \ xE9 " ,
    s2 =  " e \ u0301 " ;

s1 . normalize (). длина ;			// 1
s2 . normalize (). длина ;			// 1

[ ... s1]. длина ;					// 1
[ ... s2]. длина ;					// 2

s1 === s2;						// false
s1 ===  s2 . normalize ();			// true