1. Destructing defaults vs Parameters defaults
2. Object asign vs Destructing (для глубокого клонирования использовать второе, первый вариант хранит ссылку на объект)
3. Unnamed fn in Literals ( ... = { getValue() { // }, ... )
4. Getter / Setter (возможно деструктурирование и значение по умолчанию, но рест/спред не разрешен, т.к. у сеттера только 1 параметр)
5. Computed property name ( obj = { ["name"]: ..., // })
6. Proto (лучше использовать для этого setPrototypeOf)
7. Super (используется только  в сжатых методах)
8. Interpolation (позволяет конкатенировать строки, литералы, переменные; интерполированный литерал лексически охвачен, там где объявлен)
9. Tag interpolation (передача параметров через интерполяцию)
10. Arrow fn (стрелочные фн лучше использовать в местах где короткие выражения, иначе трудно  искать '=>' в гуще кода
11. This in arrow fn
12. For .. of
13. Unicode symbols
14. regular expression (flags, lastIndex); Sticky flags
15. Numbers
