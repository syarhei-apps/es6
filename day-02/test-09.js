function bar() {
    return function foo(strings, ...values) {
        console.log( strings );
        console.log( values );
    }
}

function test(a,...b) {
    console.log(b);
}

var desc = "awesome";

bar`Everything is ${desc}!`;
// [ "Everything is ", "!"]
// [ "awesome" ]

test`${'str'}${'tes'}`;

var amt1 = 11.99,
    amt2 = amt1 * 1.08,
    name = "Kyle";


let message = `Thanks for your purchase, $${name}! Your
product cost was $${amt1}, which with tax
comes out to $${amt2.toFixed(2)}.`;

console.log(String.raw({ raw: "test" },"","name"));