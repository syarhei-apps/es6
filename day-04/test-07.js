class MArray extends Array {
    first() {
        return this[0];
    }

    last() {
        return this[this.length-1];
    }
}

let mo = new MArray(1, 2, 3, 4, 5);
console.log(mo.last());
console.log(mo.first());