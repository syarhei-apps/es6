class Foo {
    static cool() { console.log( "cool" ); }
    wow() { console.log( "wow" ); }
}

class Bar extends Foo {
    static awesome() {
        super.cool();
        console.log( "awesome" );
    }
    neat() {
        super.wow();
        console.log( "neat" );
    }
}

Foo.cool();					// "cool"
Bar.cool();					// "cool"
Bar.awesome();				// "cool"
// "awesome"

const b = new Bar();
b.neat();					// "wow"
// "neat"

console.log(b.awesome);					// undefined
console.log(b.cool);						// undefined

debugger;