class Animal {

    constructor() {
        this.age = 10;
    }

    getAge() {
        return this.age;
    }
}

class Dog extends Animal {
    constructor() {
        debugger;
        super();
    }

    *getAge() {
        yield (this.age - 10);
        yield super.getAge();
    }

    set newAge(value) {
        this.age = value;
    }
}

let dog = new Dog();
dog.newAge = 23;

let it = dog.getAge();

for (let value of it) {
    console.log(value);
}

debugger;