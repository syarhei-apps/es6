function Hello(name) {

    let value = 0;

    function greeting() {
        value++;
        console.log( value );
    }

    function getValue() {
        return value;
    }

    // public API
    return {
        greeting, value, getValue
    };
}

const me = Hello("Kyle");

//me.greeting = () => { console.log("New fn") };

me.greeting();			// Hello Kyle!
me.greeting();			// Hello Kyle!
console.log(me.value);
console.log(me.getValue());