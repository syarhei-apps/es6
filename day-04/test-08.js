class Foo {
    constructor() {
        console.log( "Foo: ", new.target === Foo );
        console.log( "Foo this: ", new.target === this );
    }
}

class Bar extends Foo {
    constructor() {
        super();
        console.log( "Bar: ", new.target === Bar );
        console.log( "Bar this: ", new.target === this );
        let a = new.target;
    }
    baz() {
        console.log( "baz: ", new.target );
    }
}

const a = new Foo();
// Foo: Foo

const b = new Bar();
// Foo: Bar   <-- respects the `new` call-site
// Bar: Bar

b.baz();
// baz: undefined