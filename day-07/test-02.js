class MyCoolArray extends Array {
    sum() {
        return this.reduce( function reducer(acc,curr){
            return acc + curr;
        }, 0 );
    }
}

const x = new MyCoolArray(3);
x.length;						// 3 -- oops!
x.sum();						// 0 -- oops!

const y = [3];					// Array, not MyCoolArray
// y.length;						// 1
// y.sum();						// `sum` is not a function

const z = MyCoolArray.of(3);
z.length;						// 1
z.sum();						// 3

debugger;