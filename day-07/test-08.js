const o = {
    foo: 42,
    [Symbol("bar")]: "hello world",
    baz: true
};

let r = Object.getOwnPropertySymbols( o );	// [ Symbol(bar) ]

console.log(r.map(r => o[r]));