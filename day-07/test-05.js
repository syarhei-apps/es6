class MyCoolArray extends Array {
}

const x = new MyCoolArray(1, 2, 3);

MyCoolArray.from( [1, 2] ) instanceof MyCoolArray;	// true

Array.from(
    MyCoolArray.from( [1, 2] )
) instanceof MyCoolArray;							// false

debugger;