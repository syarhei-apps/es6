const arr = [ 1 , 2 , 3 , 4 , 5 ]. copyWithin ( 0, -2, -1 );

arr.fill(10, 0, 1);

let result = arr.find(v => v > 4);
result = arr.findIndex(v => v === 10);

console.log(result);

const a = [1, 2, 3];

console.log([...a.keys()]);						// [0,1,2]
console.log([...a.entries()]);					// [ [0,1], [1,2], [2,3] ]

console.log([...a[Symbol.iterator]()]);			// [1,2,3]

debugger;