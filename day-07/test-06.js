class MyCoolArray extends Array {
    // force `species` to be parent constructor
    static get [Symbol.species]() { return Array; }
}

const x = new MyCoolArray(1, 2, 3);

x.slice( 1 ) instanceof MyCoolArray;				// false
x.slice( 1 ) instanceof Array;						// true

MyCoolArray.from( x ) instanceof MyCoolArray;		// true
MyCoolArray.of( [2, 3] ) instanceof MyCoolArray;	// true

debugger;

// species параметр используется только для прототипов (slice) . Он не будет вызван для Array.of/from(...) так как они оба использует this привязку