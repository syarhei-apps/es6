var target, o1, o2, o3, o4;
target = {};
o1 = {a: 1};
o2 = {b: 2};
o3 = {c: 3, [Symbol("g")]: 3};
o4 = {d: 4};

// setup read-only property
Object.defineProperty( o3, "e", {
    value: 5,
    enumerable: true,
    writable: false,
    configurable: false
} );

// setup non-enumerable property
Object.defineProperty( o3, "f", {
    value: 6,
    enumerable: false
} );

// setup non-enumerable symbol
Object.defineProperty( o3, Symbol( "h" ), {
    value: 8,
    enumerable: false
} );

Object.setPrototypeOf( o3, o4 );

Object.assign( target, o1, o2, o3 );

console.log(target);

Object.getOwnPropertyDescriptor( target, "e" );
// { value: 5, writable: true, enumerable: true,
//   configurable: true }

Object.getOwnPropertySymbols( target );
// [Symbol("g")]


// Object.assign копирует все итерируемые поля (т.о. можно создать через defineProperty неитерируемое поле которое не будет скопировано в новый обхект
// Object.create создает объект и все  поля из второго объекта копирует в proto // Еще один способ наследования (setPrototypeOf)

const o11 = {
    foo() {
        console.log("foo");
    }
};

const proto = Object.assign({},o11);
const o21 = Object.assign(
    Object.create(o11),
    target
);

// delegates to `o1.foo()`
o21.foo();							// foo

console.log(o3);

debugger;