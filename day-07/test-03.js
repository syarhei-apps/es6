// array-like object
var arrLike = {
    length: 3,
    0: "foo",
    1: "bar"
};

var arr = Array.prototype.slice.call( arrLike );


var arr1 = Array.from( arrLike );

var arrCopy = Array.from( arr1 );

console.log(arr);
console.log(arr1);

var a = Array( 4 );								// four empty slots!

var b = Array.apply( null, { length: 4 } );		// four `undefined` values

var c = Array.from( { length: 4 } );			// four `undefined` values

debugger;