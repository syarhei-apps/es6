let c = Number.EPSILON;

console.log(c);

let v = 4.;

console.log(Number.isSafeInteger(v));

var x = Math.pow( 2, 53 ),
    y = Math.pow( -2, 53 );

console.log(Number.isSafeInteger(x));

let z = String.fromCodePoint( 0x1d49e );			// "𝒞"

let zz = "ab𝒞d".codePointAt( 2 ).toString( 16 );		// "1d49e"

var s1 = "e\u0301";
s1.length;							// 2

var s2 = s1.normalize();
s2.length;							// 1
console.log(s2 === "\xE9");						// true
s1 = s1.repeat(2);

debugger;