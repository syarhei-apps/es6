const o1 = {
    foo() {
        console.log("foo");
    }
};
const o2 = {
    // .. o2's definition ..
};

Object.setPrototypeOf( o2, o1 );

// delegates to `o1.foo()`
o2.foo();							// foo

debugger;