const th = {
    then(resolve,reject) {
        setTimeout(() => {
            resolve(this.num*2);
        }, 1000)

        // reject("Error");
    }
};
// const th = {
//     then: function thener(fulfilled) {
//         // call `fulfilled(..)` once every 100ms forever
//         setInterval(fulfilled, 100);
//     }
// };

function promise(num) {
    th.num = num;
    return Promise.resolve(th);
}

promise(5).then(result => {
    console.log(result)
}).catch(error => {
    console.log(`Error: ${error}`)
});