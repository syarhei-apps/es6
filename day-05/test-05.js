function res(num) {
    // Что будет если передать ошибку ?
    return Promise.resolve(--num);
}

function rej(num) {
    return Promise.reject(++num);
}

function wow() {
    return Promise.resolve(ror());
}

function ror() {
    return Promise.reject("EEEE BOY");
}

// (() => {
//     rej(6)
//         .then(
//             num => res(num),
//             num => {
//                 console.log(`Error 1: ${num}`);
//                 return rej(num)
//             }
//         )
//         .then(
//             result => {console.log(result)},
//             num => {
//                 console.log(`Error 2: ${num}`);
//                 return rej(num)
//             }
//         )
//         .then(
//             null,
//             error => {
//                 console.log(`Error Last ${error}`);
//             }
//         )
//         .catch(
//             error => {
//                 console.log(`Error Catch ${error}`);
//             }
//         )
// })();
//
// (() => {
//     wow()
//         .then(result => {
//             console.log(result);
//         })
//         .catch(error => {
//             console.log(error)
//         })
// })();

module.exports = {
    res,
    rej,
    wow,
    ror
};