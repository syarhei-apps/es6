class Foo {
    // defer `species` to derived constructor
    static get [Symbol.species]() { return this; }
    spawn() {
        return new this.constructor[Symbol.species]();
    }
}

class Bar extends Foo {
    // force `species` to be parent constructor
    value = 2;
    static get [Symbol.species]() { return Foo; }
}

const a = new Foo();
const b = a.spawn();
console.log(b instanceof Foo);					// true

const x = new Bar();
const y = x.spawn();
console.log(y instanceof Bar);					// false
console.log(y instanceof Foo);					// true