const {res, rej, wow, ror} = require("./test-05");

function *main() {

    let ret;
    try {
        ret = yield rej(4);
    }
    catch (err) {
        ret = yield res( err );
    }

    ret = yield res( ret );

    // step 3
    ret = yield Promise.all( [
        res( ret ),
        res( ret ),
        res( ret )
    ] );

    return res( ...ret );
}

function run(gen) {
    let args = [].slice.call(arguments, 1), it;

    it = gen.apply( this, args );

    return Promise.resolve()
        .then( function handleNext(value){
            const next = it.next(value);

            return (function handleResult(next){
                if (next.done) {
                    return next.value;
                }
                else {
                    return Promise.resolve( next.value )
                        .then(
                            handleNext,
                            function handleErr(err) {
                                return Promise.resolve(
                                    it.throw( err )
                                )
                                    .then( handleResult );
                            }
                        );
                }
            })( next );
        } );
}

run( main )
    .then(result => {
        console.log(result)
    })
    .catch(error => {
        console.log(error)
    });