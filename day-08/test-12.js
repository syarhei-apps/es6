let obj = { a: 1 },
    handlers = {
        get(target,key,context) {
            // note: target === obj,
            // context === pobj
            console.log( "accessing: ", key );
            return target[key];
        }
    },
    { proxy, revoke } =
        Proxy.revocable( obj, handlers );

let r1 = proxy.a;
// accessing: a
// 1

// later:
revoke();

let r2 = proxy.a;
// TypeError

debugger;