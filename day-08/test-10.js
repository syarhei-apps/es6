var a = [1,2,3],
    b = [4,5,6];

b[Symbol.isConcatSpreadable] = false;

let res = [].concat( a, b );		// [1,2,3,[4,5,6]]

var obj = {
    foo: 1,
    bar: 2
};

var bar = 4;

obj[Symbol.unscopables] = {
    foo: false,
    bar: true
};

with (obj) {
    console.log(foo); // 1
    console.log(bar); // ReferenceError: bar is not defined
}

debugger;