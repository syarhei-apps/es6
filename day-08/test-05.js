function Foo(greeting) {
    this.greeting = greeting;
}

Foo.prototype[Symbol.toStringTag] = "Foo";

Object.defineProperty( Foo, Symbol.hasInstance, {
    value: function(inst) {
        return inst.greeting === "hello";
    }
} );

var a = new Foo( "hello" ),
    b = new Foo( "hello" );

b[Symbol.toStringTag] = "cool";

a.toString();				// [object Foo]
String( b );				// [object cool]

let f1 = a instanceof Foo;			// true
let f2 = b instanceof Foo;			// false

debugger;