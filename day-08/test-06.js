class Cool {
    // defer `@@species` to derived constructor
    static get [Symbol.species]() { return this; }

    again() {
        return new this.constructor[Symbol.species]();
    }
}

class Fun extends Cool {}

class Awesome extends Cool {
    // force `@@species` to be parent constructor
    static get [Symbol.species]() { return Cool; }
}

const a = new Fun(),
    b = new Awesome(),
    c = a.again(),
    d = b.again();

let f1 = c instanceof Fun;			// true
let f2 = d instanceof Awesome;		// false
let f3 = d instanceof Cool;			// true

debugger;