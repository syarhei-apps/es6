const arr = [1, 2, 3, 4, 5];

let result1 = arr + 10;				// 1,2,3,4,510

arr[Symbol.toPrimitive] = () => 10;

let result2 = arr + 10;				// 25

debugger;