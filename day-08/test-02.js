(function(){  }).name;					// name:
(function*(){  }).name;				// name:
global.foo = function(){  };		// name:

class Awesome {
    constructor() {  }			// name: Awesome
    funny() {  }					// name: funny
}

var c = class Awesome {  };		// name: Awesome

var o = {
    foo() {  },					// name: foo
    *bar() {  },					// name: bar
    baz: () => {  },				// name: baz
    bam: function(){  },			// name: bam
    get qux() {  },				// name: get qux
    set fuz(v) {  },				// name: set fuz
    ["b" + "iz"]:
        function(){  },			// name: biz
    [Symbol( "buz" )]:
        function(){  }			// name: [buz]
};

var x = o.foo.bind( o );			// name: bound foo
(function(){  }).bind( o );		// name: bound

module.exports = function() {  };	// name: default

var y = new Function();				// name: anonymous
var GeneratorFunction =
    function*(){}.__proto__.constructor;
var z = new GeneratorFunction();	// name: anonymous

debugger;