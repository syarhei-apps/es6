# Meta programming

1. <b>Lexical binding </b> (это то что используется при рекурсии)

2. <b>Property 'name'. </b> (см. test 1-2). Оно не доступно для записи, но возможно
использование функции Object.defineProperty для установки поля name

3. <b>Meta properties</b>

4. <b>Well Known Symbols</b> <br />
 Symbol.Iterator (используется только для ``for of`` и ``spread``) <br />
 Symbol.toStringTag (Как будет выглядеть имя объекта приведенное к строке) <br />
 Symbol.hasInstance (используется только с ``Object.defineProperty``) <br />
 Symbol.species <br />
 Symbol.toPrimitive (Используется в случае приведения в другому типу данных
 для операций ``+`` / ``===``)
 Symbol.match <br />
 Symbol.replace <br />
 Symbol.search <br />
 Symbol.split <br />
 Symbol.isConcatSpreadable <br />
 Symbol.unscopables <br />

5. <b>Proxy!!!</b> (test-11) <br />
 Proxy.revocable <br />
 Proxy First <br />
 Proxy Last <br />
