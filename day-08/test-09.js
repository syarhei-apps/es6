var str = "hey JudE";
var re = /[A-Z]/g;
var re2 = /[.]/g;

re[Symbol.search] = function (name) {
    console.log(name);
    return 1000;
};

re2[Symbol.match] = false;

console.log(str.search(re)); // returns 4, which is the index of the first capital letter "J"
console.log(str.search(re2)); // returns -1 cannot find '.' dot punctuation

var o = { a:1, b:2, c:3 },
    a = 10, b = 20, c = 30;

o[Symbol.unscopables] = {
    a: false,
    b: true,
    c: false
};

with (o) {
    console.log( a, b, c );		// 1 20 3
}

debugger;

