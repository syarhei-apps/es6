const re = /foo/;
re[Symbol.match] = false;
let r1 = "/foo/".startsWith(re); // true
let r2 = "/baz/".endsWith(re);   // false

let reg = /str/;
reg[Symbol.replace] = function (str, res) {
    console.log(str, res);
    return str.replace(" ", res);
};

str = "sergei murkou".replace(reg, '111');

debugger;