# Meta programming

1. **Reflect** (ведут себя точно также как и ``Object.*``, только отличие в том,
что ``Object`` преобразует аргумент в объект если он таковым не является,
а Reflect выдаст ошибку)

2. **Future Testing**

3. **TCO** <br>
 PTC, Non-TCO Optimizations (``trampolining``, ``recursion unrolling``, ``self-adjusting code``)