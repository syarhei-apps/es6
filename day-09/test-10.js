async function getNames() {
    return Promise.all([
        Promise.resolve(5),
        getName(10),
        getName(50, 2000),
        getName(15, 4000),
    ]);
}

function getName(number = 0, timer = 0) {
    return new Promise((resolve) => {
        setTimeout(() => {
            resolve(number);
        }, timer);
    });
}

const cb = (x, y) => x + y;

(async function () {
    let arr = await getNames();
    let sum = arr.reduce(cb);
    console.log(sum);
})();