// Object.observe

let obj = { a: 1, b: 2 };

obj.observe(
    obj,
    v => {
        console.log(v);
        },
    ["add"]
);

obj.c = 3;