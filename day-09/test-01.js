class Name {
    constructor(name = "sergei") {
        this.name = name;
        this.value = 0;
    }

    // Count of get call of property 'name'
    get current() {
        return this.value;
    }
}

let N = new Name();
let NProxy = new Proxy(N, {
    get(target, key, context) {
        console.log(key);
        if (key === "name")
            Reflect.set(target, "value", target["value"] + 1, context);
        return Reflect.get(target,key,context);
    },
});

let a = NProxy.name;
let b = NProxy.name;

let c = NProxy.current;

debugger;