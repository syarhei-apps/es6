"use strict";

function foo(x) {
    let acc;

    function _foo() {
        if (x > 1) {
            acc = acc + (x / 2);
            x = x - 1;
            return _foo();
        }
    }

    acc = 1;

    while (x > 1) {
        try {
            _foo();
        }
        catch (err) {
            console.log(err);
        }
    }

    return acc;
}

let r = foo( 123456 );			// 3810376848.5

debugger;

// Менее читаемый , менее оптимицированный (работает медленнее предыдущих)