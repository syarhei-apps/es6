"use strict";

let TCO_ENABLED;

try {
    (function foo(x){
        if (x < 10000)
            return foo( x + 1 );
    })( 1 );

    TCO_ENABLED = true;
}
catch (err) {
    TCO_ENABLED = false;
}