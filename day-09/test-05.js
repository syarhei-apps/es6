// TCO
// Оптимизация хвоостовых вызовов

"use strict";

function foo(x) {
    return x * 2;
}

function bar(x) {
    x = x + 1;
    if (x > 10) {
        return foo( x );
    }
    else {
        return bar( x + 1 );
    }
}

bar( 5 );				// 24
bar( -15000 );				// 32