const littleEndian = (function () {
    const buffer = new ArrayBuffer(2);
    new DataView(buffer).setInt16(0, 257, true);
    return new Int16Array(buffer)[0] === 256;
})();

console.log(littleEndian);