const m = new Map();

const x = {id: 1},
    y = {id: 2};

m.set( x, "foo" );
m.set( y, "bar" );

m.get( x );						// "foo"
m.get( y );						// "bar"

console.log(m.size);

m.delete( y );

console.log(m.size);

m.clear();

console.log(m.size);

debugger;

