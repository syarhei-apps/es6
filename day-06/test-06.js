const m = {};

const x = {id: 1},
    y = {id: 2};

m[x] = "foo";
m[y] = "bar";

console.log(m[x]);							// "bar"
console.log(m[y]);							// "bar"