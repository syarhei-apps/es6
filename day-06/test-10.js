const m = new Map();

const x = {id: 1},
    y = {id: 2};

m.set( x, "foo" );
m.set( y, "bar" );

x.id = 3;

console.log(m.has( x ));						// true
console.log(m.has( y ));						// false

const vals = [...m.keys()];

console.log(vals);							// ["foo","bar"]
console.log(Array.from( m.values() ));		// ["foo","bar"]