
const buf = new ArrayBuffer(20);

console.log(buf.byteLength);

const first = new Uint16Array(buf, 0, 2)[0],
    second = new Uint8Array(buf, 2, 1)[0],
    third = new Uint8Array(buf, 3, 1)[0],
    fourth = new Float32Array(buf, 4, 4)[0];

debugger;