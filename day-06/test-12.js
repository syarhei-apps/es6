const m = new WeakMap();

let x = {id: 1},
    y = {id: 2},
    z = {id: 3},
    w = {id: 4};

m.set( x, y );

x = null;						// { id: 1 } is GC-eligible
y = null;						// { id: 2 } is GC-eligible
// only because { id: 1 } is

m.set( z, w );

w = null;						// { id: 4 } is not GC-eligible
// w = "Siarhei";
// Не изменил значение так как глубокоесвязывание
// w.id = "Siarhei"
// Изменит значение так как мы уже обращаемся не к переменной w а к той области памяти которая содержит это значение и меняя его , мы меняем и все места куда он ссылается
console.log(m.get(z));

// Map обладает 

debugger;