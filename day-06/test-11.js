const m = new WeakMap();

const x = {id: 1},
    y = {id: 2};

m.set( x, "foo" );

x.id = 3;

console.log(m.has( x ));						// true
console.log(m.has( y ));						// false
