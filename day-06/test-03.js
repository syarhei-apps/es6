const buf = new ArrayBuffer(2);

const view8 = new Uint8Array(buf);
const view16 = new Uint16Array(buf);

view16[0] = 3085;
console.log(view8[0]);						// 13
console.log(view8[1]);						// 12

console.log(view8[0].toString( 16 ));		// "d"
console.log(view8[1].toString( 16 ));		// "c"

// swap (as if endian!)
const tmp = view8[0];
view8[0] = view8[1];
view8[1] = tmp;

console.log(view16[0]);						// 3340