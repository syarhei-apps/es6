function *generaltor(num) {
    yield 3;
    throw 4;
    return 5;
}

let it = generaltor(4);
try {
    console.log(it.next());
    console.log(it.next());
} catch (err) {
    console.log(`Error $${err}`);
}

console.log(it.next());