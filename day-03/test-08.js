function *foo() {
    var x = yield 1;
    var y = yield 2;
    var z = yield 3;
    console.log( x, y, z );
}

var it = foo();

console.log(it.next("fwagawg"));
console.log(it.next("fwa"));
console.log(it.next("gwagwwa"));
console.log(it.next("gawgwa"));

