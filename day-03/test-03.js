var it = {
    // make the `it` iterator an iterable
    [Symbol.iterator]() { return this; },

    next() {
        return {value: 1, done: true };
    },
};

it[Symbol.iterator]() === it;		// true


for (var v of it) {
    console.log( v );
}
