function *test() {
    yield "before set-time-out";
    setTimeout(function*() {
        yield 5;
    }, 3000);
    yield "after set-time-out";
}

let it = yield test();
console.log(it.next());
console.log(it.next());
console.log(it.next());
