let arr = [1, 2, 3];

let it = arr[Symbol.iterator]();

let val = it.next();		// { value: 1, done: false }
it.next();		// { value: 2, done: false }
it.next();		// { value: 3, done: false }

it.next();		// { value: undefined, done: true }

console.log(val);

let result = arr.iterator(Symbol("key"));

console.log(result);