// for (var i of 3) {
//     console.log( i );
// }
// // 0 1 2 3
//
// [...-3];				// [0,-1,-2,-3]

let a = {x:1, y:2};
let it = a[Symbol.iterator]();

let [x,y] = it;

console.log(x,y);