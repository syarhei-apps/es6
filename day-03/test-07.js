function *foo(x) {
    if (x < 3) {
        x = yield *foo( x + 1 );
    }
    return x * 2;
}

let it = foo( 1 );
console.log(it.next());

