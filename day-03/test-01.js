const symbol = Symbol("Test Symbol");

console.log(symbol.toString());

console.log(typeof symbol);

const symObj = Object(symbol);

console.log(symObj.toString());

console.log(typeof symObj);

console.log(symObj instanceof Symbol);

let obj = {
    [symObj]: "test"
};

obj = {
    iterator: 1,
    [Symbol.iterator]: function() {
        debugger;
    }
};

// один символ в объекте
console.log( Object.getOwnPropertySymbols(obj)[0].toString() ); // Symbol(Symbol.iterator)

debugger;